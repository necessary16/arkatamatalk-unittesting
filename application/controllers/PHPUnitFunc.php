<?php

use PHPUnit\Framework\TestCase; 

class UnitTest extends TestCase
{
    public function countWords($sentence)
    {
        return count(explode(" ", $sentence));
    }

    public function calculateDiscount($discount, $originalPrice)
    {
        $discountPrice =  $originalPrice * $discount / 100;
        return $originalPrice - $discountPrice;
    }

    public function tambah($a, $b) 
    {
        return $a + $b;
    }

    public function kali ($a, $b)
    {
        return $a * $b;
    }

    public function kurang($a, $b)
    {
        return $a - $b;
    }

    public function bagi($a, $b)
    {
        return $a / $b;
    }

    public function kalitambah($a, $b, $c)
    {
        return $a + $b * $c;
    }

}