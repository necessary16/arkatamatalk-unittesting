<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestUnit extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }
    
    // // public function isInt()
    // // {
    // //     echo $this->unit->run(25, 'is_int');
    // // }

    // public function isString()
    // {
    //     echo $this->unit->run("Syahir", 'is_string');
    // }

    private function calculateDiscount($discount, $originalPrice)
    {
        $discountPrice =  $originalPrice * $discount / 100;
        return $originalPrice - $discountPrice;
    }

    public function testDiscount()
    {
        $test = $this->calculateDiscount(30,1000);
        $expected_result = 700;
        $test_name = 'Menghitung Diskon';

        echo $this->unit->run($test, $expected_result, $test_name);
    }

    // private function tambah($a, $b)
    // {
    //     return $a + $b;
    // }

    // private function kali($a, $b)
    // {
    //     return $a * $b;
    // }

    // private function kurang($a, $b)
    // {
    //     return $a - $b;
    // }

    // private function bagi($a, $b)
    // {
    //     return $a / $b;
    // }

    // private function kalitambah($a, $b, $c)
    // {
    //     return $a + $b * $c;
    // }

    // public function testCalculator()
    // {
    //     $test = $this->tambah(2, 4);
    //     $expected_result = 6;
    //     $test_name = 'Menambah';

    //     $this->unit->run($test, $expected_result, $test_name);

    //     $test = $this->kali(2, 4);
    //     $expected_result = 8;
    //     $test_name = 'Kali';

    //     $this->unit->run($test, $expected_result, $test_name);

    //     $test = $this->kalitambah(2, 4, 6);
    //     $expected_result = 26;
    //     $test_name = 'Kali Tambah';

    //     $this->unit->run($test, $expected_result, $test_name);

    //     echo $this->unit->report();

    // }

}