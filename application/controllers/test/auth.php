<?php

class Auth extendS CI_Controller
{
    public function index ()
    {
        show_404();
    }

    public function login ()
    {
        $this->load->model('auth_model');
        $this->load->library('form_validation');

        $rules = $this->auth_model->rules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE)
        {
            return $this->load->view('login_form');
        }

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if($this->auth_model->login($username,$password))
        {
            redirect('admin');
        } else {
            $this->session->set_flashdata('message_login_error', 'Login Gagal');   
        }

        $this->load->view('login_form');
    }

    public function logout()
    {
        $this->load->model('auth_model');
        $this->auth_model->logout();

        redirect(site_url());
    }
}


class TestLogin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('unit_test');
    }

    public function testLoginBerhasil()
    {

    }

    public function testDiscount()
    {
        $test = $this->calculateDiscount(50,1000);
        $expected_result = 500;
        $test_name = 'Menghitung Diskon';

        echo $this->unit->run($test, $expected_result, $test_name);
    }
}