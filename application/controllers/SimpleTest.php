<?php


use PHPUnit\Framework\TestCase;

require_once "PHPUnitFunc.php";

class SimpleTest extends TestCase
{

    // public function testInt()
    // {
    //     $this->assertIsInt(49);
    // }

    // public function testFloat()
    // {
    //     $this->assertIsFloat(4.5);
    // }

    // public function testNotFloat()
    // {
    //     $this->assertIsNotFloat("Ini float");
    // }

    // public function testString()
    // {
    //     $this->assertIsString("Syahir");
    // }

    // public function testPositiveTestcaseForAssertContains() 
    // { 

    //     $testArray = array("name"=>"binsar", "b" =>"value b"); 

    //     $value = "binsar";  
    //     // assert function to test whether 'value' is a value of array 
    //     $this->assertContains($value, $testArray, "testArray doesn't contains value as value") ; 
    // } 


    // public function testPositiveForassertIsArray()
    // {  
    // $variable  = [1,2];
    //     // Assert function to test whether assert
    //     //  variable is array or not
    //     $this->assertIsArray($variable, "assert variable is array or not");
    // }

    // public function testPositiveTestcaseForassertIsObject() 
    // { 
    //     $actualcontent = (object) ('lovely laptop');
    
    //     // Assert function to test whether given 
    //     // actual content is object or not
    //     $this->assertIsObject($actualcontent, "actual content is object or not"); 
    // }


    // public function testCountWords()
    // {
    //     $Wc = new UnitTest();

    //     $TestSentence = "My Name is binsar dan saya";
    //     $WordCount = $Wc->countWords($TestSentence);

    //     $this->assertEquals(6, $WordCount);
    // }

    // public function testDiscount() 
    // {
    //     $dc = new UnitTest();

    //     $Discount = $dc->calculateDiscount(50, 1000);

    //     $this->assertEquals(500, $Discount);
    // }

    public function testTambah()
    {
        $tambah = new UnitTest();

        $hasiltambah = $tambah->tambah(5, 5);
        $this->assertEquals(10, $hasiltambah);
    }

    public function testKurang()
    {
        $kurang = new UnitTest();

        $hasilkurang = $kurang->kurang(20, 5);
        $this->assertEquals(15, $hasilkurang);
    }
    
    public function testKali()
    {
        $kali = new UnitTest();

        $hasilkali = $kali->kali(5, 5);
        $this->assertEquals(25, $hasilkali);
    }

    public function testBagi()
    {
        $bagi = new UnitTest();

        $hasilbagi = $bagi->bagi(36, 6);
        $this->assertEquals(6, $hasilbagi);
    }

    public function testKaliTambah()
    {
        $kalitambah = new UnitTest();

        $hasilkalitambah = $kalitambah->kalitambah(2, 4, 6);
        $this->assertEquals(26, $hasilkalitambah);
    }

}

// run vendor\bin\phpunit --bootstrap vendor/autoload.php application/controllers/SimpleTest.php